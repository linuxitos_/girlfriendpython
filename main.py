import tkinter as tk
from tkinter import *
from tkinter import messagebox
from random import *
class App(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.pack()
        self.lbl1 = Label(master, text='¿Quieres ser mi novia?')
        self.lbl1.place(x=180, y=50)
        self.btnSi = Button(master, text='Sí', command=self.siQuiero)
        self.btnSi.place(x=100, y=150)
        self.btnNo = Button(master, text='No', command=self.noQuiero)
        self.btnNo.place(x=200, y=150)
    def callback(self):
        messagebox.showinfo("Salir", "Ijole, creo que eso sí no se va a poder")
    def siQuiero(self):
        messagebox.showinfo("Gracias", "Te amo! Yo programaré por ti")
        myapp.master.destroy()
    def noQuiero(self):
        myapp.btnSi.config(text='No')
        myapp.btnNo.config(text='Sí')
        messagebox.showinfo("Iuuuju", "Vez como sí quieres")
        myapp.master.destroy()
    def on_start_hover(self):
        w = self.master.winfo_width()
        h = self.master.winfo_height()
        self.btnNo.place(x=randint(0, w-80), y=randint(0, h-50))
myapp = App()
myapp.master.title("Hoy es el día")
myapp.master.maxsize(1000, 400)
myapp.master.minsize(500, 400)
myapp.master.protocol("WM_DELETE_WINDOW", myapp.callback)
myapp.btnNo.bind("<Enter>", lambda event: myapp.on_start_hover())
myapp.mainloop()