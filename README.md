# Girlfriend Python
Es una aplicación simple realizada en Python y Tkinter para pedirle a esa persona si quiere ser tu novi@ sin embargo tiene unos pequeños trucos que intenta no permitir que la otra persona diga que no.

![Login.](images/1.gif)

## Descripción

Éste pequeño script en python formó parte del aprendizaje del autor, en su proceso de aprender a programar en python y usar la librería tkinter para la creación de interfaces gráficas, por lo que podría tener errores, cambios, o se puede realizar de una manera más simple.

Cualquier duda o sugerencia por favor dejarla en los comentarios.

## Características
================
- Python 3.10
- Tkinter


#### Developed By
----------------
 * linuxitos - <contact@linuxitos.com>